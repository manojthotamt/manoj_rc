$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:/C:/Users/Manoj/eclipse-workspace/Gherkin_FW/src/main/java/code_features/REST.feature");
formatter.feature({
  "name": "REST Method\u0027s initiation and Response Validations",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Get all products, sort by highest price (descending) (working)",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Path \"http://localhost:3030/products?$sort[price]\u003d-1\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefinitions.Path(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Method \"Get\"",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefinitions.Method(String)"
});
formatter.write("\nMethod name: Get");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Statuscode \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitions.statuscode(String)"
});
formatter.write("\nStatus Code Assertion: 200");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Match header Content-type \u003d\u003d \"application/json; charset\u003dutf-8\"",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.header(String)"
});
formatter.write("\nHeader value Assertion: application/json; charset\u003dutf-8");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Match jsonpath \"data[0].id\" contains \"9999848\"",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.jsonpath(String,String)"
});
formatter.write("\nJSon Path Value Assertion: 9999848");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Print Response Time",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.Print_Response_Time()"
});
formatter.write("\nResponse Time is: 4871");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Print Response",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.Response()"
});
formatter.write("\nActual Response: {\n    \"total\": 141,\n    \"limit\": 10,\n    \"skip\": 0,\n    \"data\": [\n        {\n            \"id\": 9999848,\n            \"name\": \"New MT Product\",\n            \"type\": \"Hard 1 Good\",\n            \"price\": 904,\n            \"upc\": \"866367452\",\n            \"shipping\": null,\n            \"description\": \"This is a new MT product.\",\n            \"manufacturer\": null,\n            \"model\": \"NP124455\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-08-07T09:28:40.259Z\",\n            \"updatedAt\": \"2019-08-07T09:28:40.259Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999847,\n            \"name\": \"New MT Product\",\n            \"type\": \"Hard 1 Good\",\n            \"price\": 674,\n            \"upc\": \"893830458\",\n            \"shipping\": null,\n            \"description\": \"This is a new MT product.\",\n            \"manufacturer\": null,\n            \"model\": \"NP124455\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-08-07T09:11:32.543Z\",\n            \"updatedAt\": \"2019-08-07T09:11:32.543Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999842,\n            \"name\": \"New MT Product\",\n            \"type\": \"Hard 1 Good\",\n            \"price\": 321,\n            \"upc\": \"173487961\",\n            \"shipping\": null,\n            \"description\": \"This is a new MT product.\",\n            \"manufacturer\": null,\n            \"model\": \"NP124455\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-08-06T14:16:06.015Z\",\n            \"updatedAt\": \"2019-08-06T14:16:06.015Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999909,\n            \"name\": \"Manoj\",\n            \"type\": \"Hard MTT Goods\",\n            \"price\": 144.41,\n            \"upc\": \"4974649424\",\n            \"shipping\": null,\n            \"description\": \"This is a new MT products.\",\n            \"manufacturer\": null,\n            \"model\": \"NP1224455\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-08-16T14:49:57.912Z\",\n            \"updatedAt\": \"2019-08-19T11:11:23.316Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999894,\n            \"name\": \"New MT Product\",\n            \"type\": \"Hard 1 Good\",\n            \"price\": 110,\n            \"upc\": \"721089443\",\n            \"shipping\": null,\n            \"description\": \"This is a new MT product.\",\n            \"manufacturer\": null,\n            \"model\": \"NP124455\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-08-14T12:46:33.545Z\",\n            \"updatedAt\": \"2019-08-14T12:46:33.545Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999786,\n            \"name\": \"New Product\",\n            \"type\": \"Hard Good\",\n            \"price\": 99.99,\n            \"upc\": \"12345676\",\n            \"shipping\": null,\n            \"description\": \"This is a placeholder request for creating a new product.\",\n            \"manufacturer\": null,\n            \"model\": \"NP12345\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-07-09T13:26:44.784Z\",\n            \"updatedAt\": \"2019-07-09T13:26:44.784Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999791,\n            \"name\": \"New Product\",\n            \"type\": \"Hard Good\",\n            \"price\": 99.99,\n            \"upc\": \"12345676\",\n            \"shipping\": null,\n            \"description\": \"This is a placeholder request for creating a new product.\",\n            \"manufacturer\": null,\n            \"model\": \"NP12345\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-07-10T06:54:58.204Z\",\n            \"updatedAt\": \"2019-07-10T06:54:58.204Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999792,\n            \"name\": \"New Product\",\n            \"type\": \"Hard Good\",\n            \"price\": 99.99,\n            \"upc\": \"12345676\",\n            \"shipping\": null,\n            \"description\": \"This is a placeholder request for creating a new product.\",\n            \"manufacturer\": null,\n            \"model\": \"NP12345\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-07-10T06:55:08.448Z\",\n            \"updatedAt\": \"2019-07-10T06:55:08.448Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999745,\n            \"name\": \"manoj\",\n            \"type\": \"Hard MT Goods\",\n            \"price\": 49,\n            \"upc\": \"1562593689829\",\n            \"shipping\": null,\n            \"description\": \"This is a placeholder request for creating a new MT products.\",\n            \"manufacturer\": null,\n            \"model\": \"NP12244\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-07-08T13:48:09.935Z\",\n            \"updatedAt\": \"2019-07-08T13:48:09.935Z\",\n            \"categories\": [\n                \n            ]\n        },\n        {\n            \"id\": 9999929,\n            \"name\": \"manoj\",\n            \"type\": \"Hard MT Goods\",\n            \"price\": 49,\n            \"upc\": \"1566803921694\",\n            \"shipping\": null,\n            \"description\": \"This is a placeholder request for creating a new MT products.\",\n            \"manufacturer\": null,\n            \"model\": \"NP12244\",\n            \"url\": null,\n            \"image\": null,\n            \"createdAt\": \"2019-08-26T07:18:41.729Z\",\n            \"updatedAt\": \"2019-08-26T07:18:41.729Z\",\n            \"categories\": [\n                \n            ]\n        }\n    ]\n}");
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});