package code_features;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.cucumber.core.api.Scenario;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.Header;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import code_features.RestMethods;
import code_features.Soap_Services;

import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.JsonElement;

import static io.restassured.RestAssured.given;

public class StepDefinitions {

	static RestMethods Rest = new RestMethods();
	static Soap_Services Soap_Methods = new Soap_Services();
	static JSONObject RequestParameters = new JSONObject();
	static JSONObject UpdatedRequestParameters = new JSONObject();
	
	public static String EndPath;
	public static String FilePath;
	public static Response RESTResponse;
	public static Header headerKeyValue;
	public static String jsonString;
	public static RequestSpecification spec;
	public static Object Recent_id;
	public static String Temp_Variable;
	
	private Scenario scenario;
	
//	@Before()
//	public void before_scenario(Scenario scenario) {
//		this.scenario = scenario;
//	//	System.out.println("\nInitiate http://localhost:3030/products\n");
//	}
//	
//	@After()
//	public void after_scenario() {
//		System.out.println("\nClose http://localhost:3030/products\n");
//	}
//	
//	@Before("@First")
//	public void before_scenario1() {
//		System.out.println("\n+++++++++++++++++++Initiate http://localhost:3030/products\n");
//	}
//	
//	@After("@First")
//	public void after_scenario1() {
//		System.out.println("\n+++++++++++++++++Close http://localhost:3030/products\n");
//	}

	@Given("Path {string}")
	public String Path(String endPath) {

		EndPath = endPath;
		return EndPath;
	}
	
//	@Given("i have Product parameters as")
//	public void i_have_Product_parameters_as(DataTable dt) throws Exception {
//		List<List<String>> outerList = dt.
//		  for(List<String> innerList : outerList)
//		    {
//		      System.out.println(innerList.get(0));
//		    }
//	}
	
	@When("service is {string} with XML file path: {string}")
	public void Soap_Method(String service, String xmlfilepath) throws Exception {
		
		RESTResponse = Soap_Methods.soapmethod(EndPath, xmlfilepath);
	}

	@When("Method {string}")
	public void Method(String Method_Name) {

		System.out.println("\nMethod name: " + Method_Name);
	//	scenario.write("\nMethod name: " + Method_Name);
		switch (Method_Name) {
		case "Get":
			RESTResponse = Rest.Get(EndPath);
			break;

		case "Delete":
			RESTResponse = Rest.Delete(EndPath);
			break;
			
		case "Post":
			System.out.println("Post post");
			RESTResponse = Rest.POST_with_File(EndPath, spec, RequestParameters);
			break;
		}
	}
	
	@And("request paramets are:")
	public String request_params_are(List requestparams) {
	//	JSONObject reqparam = new JSONObject();
		System.out.println("in request paramets are: method");
		ObjectMapper mapper = new ObjectMapper();
	//	String jsonString = mapper.writeValueAsString(requestparams);
	//	String js = new String(requestparams);
	//	String jj = new Gson().toJson(requestparams);
	//	JsonElement js = new Gson();
		System.out.println(jsonString);
		return jsonString;
	}
	
	@And("parameter {string} with value {string}")
	public JSONObject request_file_formation(String key, String value) {
		
		RequestParameters.put(key, value);
//		JSONArray Request_Data = new JSONArray();
//		Request_Data.add(RequestParameters);
//		try (FileWriter file = new FileWriter(".\\Gherkin_FW\\resources\\Dynamic.json")) {
//			 
//            file.write(RequestParameters.toJSONString());
//            file.flush();
// 
//        } catch (IOException e) {
//            e.printStackTrace();
 //       }
		System.out.println("\n"+RequestParameters);
		return RequestParameters;
	}
	
	@And("parameter {string} with value {double}")
	public JSONObject request_file_formation(String key, Double value) {
		
		RequestParameters.put(key, value);
//		JSONArray Request_Data = new JSONArray();
//		Request_Data.add(RequestParameters);
//		try (FileWriter file = new FileWriter(".\\Gherkin_FW\\resources\\Dynamic.json")) {
//			 
//            file.write(RequestParameters.toJSONString());
//            file.flush();
// 
//        } catch (IOException e) {
//            e.printStackTrace();
 //       }
		System.out.println("\n"+RequestParameters);
		return RequestParameters;
	}
	
	@And("header {string} with value {string}")
	public RequestSpecification header_key_value(String HeaderKey, String HeaderValue) {
		
		spec = given().header(HeaderKey, HeaderValue);
		return spec;
		
	//	String head=HeaderKey+", "+HeaderValue;
	//	headerKeyValue=head;
	//	String headerValue=HeaderValue;
	//	System.out.println(headerKeyValue);
	//	return headerKeyValue;
				
	//	HeaderParameters.put(key, value);
	//	System.out.println("\n"+HeaderParameters);
	//	return HeaderParameters;
	}
	
	@And("Get recently created Product details")
	public Object Recently_created_Product_details() {
		Recent_id = RESTResponse.getBody().jsonPath().getJsonObject("id").toString();
		System.out.println("\nRecently created Product details: ");
		System.out.println(RESTResponse.prettyPeek());
		return Recent_id;
	}
	
	@And("get value of {string} and store in {string}")
	public String getting_recent_value(String JSON_Path, String Local_Temp_Variable) {
		
		Local_Temp_Variable = RESTResponse.getBody().jsonPath().getJsonObject(JSON_Path).toString();
		Temp_Variable = Local_Temp_Variable;
		return Temp_Variable;
	}
	
	@When("Method {string} appended with recent ID")
	public void Method_Appended(String Method_Name) {
		
		System.out.println("\nMethod name with appended value: " + Method_Name);
		switch (Method_Name) {
		case "Get":
			RESTResponse = Rest.Get(EndPath+"/"+Temp_Variable);
			break;
		}
	}
	
	@And("parameter {string} with new value {string}")
	public JSONObject Updated_file_formation(String key, String value) {
		
		UpdatedRequestParameters.put(key, value);
		System.out.println("\n Updated: "+UpdatedRequestParameters);
		return UpdatedRequestParameters;
	}
	
	@And("{string} recently created Product with above details")
	public void Patching_with_updated_Params(String Method_Name) {
		
		System.out.println("\nUpdate using Method: " + Method_Name);
		switch (Method_Name) {
		case "Patch":
			RESTResponse = Rest.Patch(EndPath+"/"+Recent_id, UpdatedRequestParameters);
			break;

//		case "Put":
//			RESTResponse = Rest.Put_with_file(EndPath, file_path);
//			break;

		}
	}
	
	@And("Print updated product details")
	public void Print_Updated_Response() {
		System.out.println("\n=================Printing Updated Response initiated=================");
		System.out.println(RESTResponse.prettyPeek());
	}
	
	@And("{string} recently created Product")
	public void Delete_recent_product(String Method_Name) {
		
		System.out.println("\nMethod name: " + Method_Name);
		//	scenario.write("\nMethod name: " + Method_Name);
			switch (Method_Name) {
			case "Delete":
				RESTResponse = Rest.Delete(EndPath+"/"+Recent_id);
				break;
			}
	}
	
	@And("header {string} with value {string} and {string} with value {string}")
	public RequestSpecification header_key_value(String HeaderKey1, String HeaderValue1, String HeaderKey2, String HeaderValue2) {
		
		spec = given().header(HeaderKey1, HeaderValue1).and().header(HeaderKey2, HeaderValue2);
		return spec;
	}
	
	@And("body parameter {string} with value {string}")
	public RequestSpecification body_parameter_key_value(String BodyParamKey, String BodyParamValue) {
		
	//	ObjectMapper mapp = new ObjectMapper();
	//	String str = ((Object) mapp).asString(BodyParamValue);
	//	spec = given().formParameter(BodyParamKey, BodyParamValue);
		spec = given().formParam(BodyParamKey, BodyParamValue);
		System.out.println(spec);
		return spec;
	}
	

	@When("Method {string} and file {string}")
	public void Method_and_file(String Method_Name, String file_path) {

		System.out.println("\nMethod name: " + Method_Name);
		switch (Method_Name) {
		case "Post":
//			RESTResponse = Rest.POST_with_File(EndPath, file_path);
			break;

		case "Put":
			RESTResponse = Rest.Put_with_file(EndPath, file_path);
			break;

		}
	}

	@Then("Statuscode {string}")
	public void statuscode(String ExpectedStatusCode) {

		System.out.println("\n=================Status Code Assertion initiated=================");
		String ActualStatusCode = String.valueOf(RESTResponse.andReturn().statusCode());
	//	System.out.println("Expected Status Code: " + ExpectedStatusCode);
	//	System.out.println("Actual Status Code: " + ActualStatusCode);
	//	System.out
	//			.println("Status code Matching (true/false): " + Objects.equals(ActualStatusCode, ExpectedStatusCode));
		assertEquals("Status code Validation:",ExpectedStatusCode,ActualStatusCode);
		scenario.write("\nStatus Code Assertion: " + ActualStatusCode);
	}

	@And("Match header Content-type == {string}")
	public void header(String ExpectedHeaderValue) {

		System.out.println("\n=================Header value Assertion initiated=================");
		String ActulaHeaderValue = RESTResponse.getHeader("Content-type");
	//	System.out.println("Expected Header value: " + ExpectedHeaderValue);
	//	System.out.println("Actual Header value: " + ActulaHeaderValue);
	//	System.out.println(
	//			"Header value Matching (true/false): " + Objects.equals(ActulaHeaderValue, ExpectedHeaderValue));
		assertEquals("Header value Validation:",ExpectedHeaderValue,ActulaHeaderValue);
		scenario.write("\nHeader value Assertion: " + ActulaHeaderValue);
	}

	@And("Match jsonpath {string} contains {string}")
	public void jsonpath(String JSonPath, String ExpectedJSonPathValue) {

		System.out.println("\n=================JSon Path Value Assertion initiated=================");
		Object ActualJSonPathValue = RESTResponse.getBody().jsonPath().getJsonObject(JSonPath).toString();
	//	System.out.println("Expected JSon Path Value: " + ExpectedJSonPathValue);
	//	System.out.println("Actual JSon Path Value: " + ActualJSonPathValue);
	//	System.out.println("UPC Matching (true/false): " + Objects.equals(ActualJSonPathValue, ExpectedJSonPathValue));
		assertEquals("Header value Validation:",ExpectedJSonPathValue,ActualJSonPathValue);
		scenario.write("\nJSon Path Value Assertion: " + ActualJSonPathValue);
	}
	
	@And("Match XMLPath {string} contains {string}")
	public void XMLPath(String xmlpath, String ExpectedXMLPathValue) {
		
		System.out.println("\n=================XML Path Value Assertion initiated=================");
		XmlPath xml = new XmlPath(RESTResponse.asString());
		String ActualXMLPathValue = xml.getString(xmlpath);
	//	System.out.println("Expected XML Path Value is: " + ExpectedXMLPathValue);
	//	System.out.println("Actual XML Path Value is: " + ActualXMLPathValue);
	//	System.out.println("XML Path Value Matching (true/false): " + Objects.equals(ActualXMLPathValue, ExpectedXMLPathValue));
		assertEquals("Header value Validation:",ExpectedXMLPathValue,ActualXMLPathValue);
	}

	@And("Print Response Time")
	public void Print_Response_Time() {

		System.out.println("\n=================Response Time is:=================");
		System.out.println(RESTResponse.getTime());
		scenario.write("\nResponse Time is: " + RESTResponse.getTime());
	}
	
	@And("assert response time is less than {long} milliseconds")
	public void Response_Time_Assertion(long ExpectedTime) {

		System.out.println("\n=================Response Time Assertion initiated=================");
		long responsetime = RESTResponse.getTime();
		if (responsetime <= ExpectedTime) {
			System.out.println("Response time is within the expected time. Actual Response Time is: " + responsetime);
		} else {
			System.out.println("Assertion filed due to Response time is greater then expected time. Actual Response Time is: " + responsetime);
		}
	}

	@And("Print Response")
	public void Response() {

		System.out.println("\n=================Printing Actual Response initiated=================");
		System.out.println(RESTResponse.prettyPeek());
//		scenario.write("\nActual Response: " + RESTResponse.prettyPrint());
	}
	
	@Then("print response to file {string}")
    public void print_response_to_file(String path) throws IOException  {
         Response  responbody = (Response) RESTResponse.getBody().asInputStream();
  //   JSONParser parser = new JSONParser();
   //     JSONObject json = (JSONObject) parser.parse(Response);
  //       Gson g = new Gson();
  //       Player p = g.fromJson(jsonString, Player.class)
    //     String str = g.toString(responbody);
      //  		 .toJson(responbody);
 //          JSONObject jobj = new JSONObject(responbody);
           File f = new File(path);
           boolean Exists = f.exists();
           if (Exists) {
                  System.out.println("Printing response to File");
                  this.write(f,responbody);
           } else {
                  f.createNewFile();
                  System.out.println("New file created");
                  this.write(f,responbody);
           }

    }
    
    private void write(File f, Response responbody) {
           try {
                  FileOutputStream f1 = new FileOutputStream(f);
                  ObjectOutputStream o = new ObjectOutputStream(f1);
                  o.writeObject(responbody);
                  o.close();
                  f1.close();
           } catch (FileNotFoundException e) {
                  System.out.println("File not found");
           } catch (IOException e) {
                  System.out.println("Error initializing stream");
           }
    }


	@Then("File compare {string} with {string}")
	public void file_compare_with(String string, String string2) throws IOException {
		File f1 = new File(string);
		File f2 = new File(string2);
		boolean isTwoEqual = FileUtils.contentEquals(f1, f2);
		assertTrue("File Comparison", isTwoEqual);
	}
}
