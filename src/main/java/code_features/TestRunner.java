package code_features;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

@RunWith(Cucumber.class)
@CucumberOptions(
  //  features = "C:\\Users\\Manoj\\eclipse-workspace\\Gherkin_FW\\src\\main\\java\\code_features\\REST.feature",
	features = "C:\\Users\\Manoj\\eclipse-workspace\\Gherkin_FW\\src\\main\\java\\code_features\\REST.feature",
    glue= {"code_features"},
    monochrome = true,
 //   plugin = { "pretty", "html:target/cucumber-html-reports", "json:target/cucumber.json" }
    plugin = { "pretty", "html:Reports/cucumber-html-reports", "json:Reports/cucumber.json" }
    )
public class TestRunner {

    @AfterClass
    public static void writeExtentReport(){
         String OutputPath= "C:\\Users\\Manoj\\eclipse-workspace\\Gherkin_FW\\Reports";     
         Collection<File> jsonFiles = FileUtils.listFiles(new File(OutputPath), new String[] {"json"}, true);
         List<String> jsonPaths =new ArrayList<String>(jsonFiles.size());
         jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
         Configuration config = new Configuration(new File("Reports"),"demo");
         ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
         reportBuilder.generateReports();  
    }
}