package code_features;

import static io.restassured.RestAssured.given;
import java.io.File;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import gherkin.deps.com.google.gson.JsonElement;
import io.cucumber.core.api.Scenario;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestMethods {
	
	@Test
	public Response Get(String path) {
		System.out.println("\n=====================Get method Initiated=====================");
		Response response = given().when().get(path);
		return response;
	}

	@Test
	public Response POST_with_File(String EndPath, RequestSpecification spec, JSONObject requestParameters) {
		System.out.println("\n=====================POST_with_File method Initiated=====================");
	//	File file_path = new File(FilePath);
		Response response = spec.body(requestParameters).when().post(EndPath);
	//	Response response = spec.when().post(EndPath);
	//	System.out.println("after post "+ response);
		return response;
	}

	@Test
	public Response Put_with_file(String EndPath, String FilePath) {
		System.out.println("\n=====================Put_with_file method Initiated=====================");
		File file_path = new File(FilePath);
		Response response = given().header("Content-Type", "application/json").body(file_path).when().put(EndPath);
		return response;
	}
	
	@Test
	public Response Patch(String EndPath, JSONObject updatedRequestParameters) {
		System.out.println("\n=====================Patch method Initiated=====================");
//		File file_path = new File(FilePath);
		Response response = given().header("Content-Type", "application/json").body(updatedRequestParameters).when().patch(EndPath);
		return response;
	}

	@Test
	public Response Delete(String EndPath) {
		System.out.println("\n=====================Delete method Initiated=====================");
		Response response = given().header("Content-Type", "application/json").when().delete(EndPath);
		return response;
	}

}
