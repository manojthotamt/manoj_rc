package code_features;

import java.io.FileInputStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class Soap_Services {

	@Test
	public Response soapmethod(String WSDlEndPath, String xmlfilepath) throws Exception {

		FileInputStream fileinputstream = new FileInputStream(xmlfilepath);

		RestAssured.baseURI = WSDlEndPath;

		Response response = given().header("Content-Type", "text/xml").and()
				.body(IOUtils.toString(fileinputstream, "UTF-8")).when().post("");

		return response;

//		XmlPath xmlpath = new XmlPath(response.asString());
//		String addresult = xmlpath.getString("AddReslut");
//		System.out.println("\nAdd reslut is: " + addresult);

	}
}