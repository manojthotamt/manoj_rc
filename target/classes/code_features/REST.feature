Feature: REST Method's initiation and Response Validations 

#Background:
#    * url "http://www.dneonline.com/calculator.asmx"
#
#    * def intA = "4"
#    * def intB = "6"
#    * def addition = read(".\Gherkin_FW\resources\Soap.xml")
#* replace addition
#  | token            | value               |
#  | #(inta)      	 | integer A       	   |
#  | #(intb)          | integer B           |
  
#Scenario: Soap Service
#	Given Path url
#	When service is "SOAP" with XML file path: addition
#	And Print Response

Scenario: Request Chaining from one to one (working)
	Given Path "http://localhost:3030/products"
	And header "Content-Type" with value "application/json"
	And parameter "name" with value "Manoj"
	And parameter "type" with value "Hard MTT Goods"
	And parameter "upc" with value "9898787848"
	And parameter "price" with value 44.66
	And parameter "description" with value "This is a new MT products."
	And parameter "model" with value "NP12244"
	When Method "Post"
	And Print Response
	And get value of "id" and store in "RecentID"
	When Method "Get" appended with recent ID
	And Print Response

#Scenario: Request Chaining End_to_End (working)
#	Given Path "http://localhost:3030/products"
#	And header "Content-Type" with value "application/json"
#	And parameter "name" with value "Manoj"
#	And parameter "type" with value "Hard MTT Goods"
#	And parameter "upc" with value "9898787848"
#	And parameter "price" with value 44.66
#	And parameter "description" with value "This is a new MT products."
#	And parameter "model" with value "NP12244"
#	When Method "Post"
#	And Print Response
#	And Get recently created Product details
#	And parameter "name" with new value "New Manoj"
#	And parameter "description" with new value "Updated description"
#	And "Patch" recently created Product with above details
#	And Print updated product details
#	And "Delete" recently created Product
#	And Print Response

#Scenario: Dynamic headers (working)
#	Given Path "http://localhost:3030/products"
#	And header "Content-Type" with value "application/json"
#	And parameter "name" with value "Manoj"
#	And parameter "type" with value "Hard MTT Goods"
#	And parameter "upc" with value "9898787848"
#	And parameter "price" with value 44.66
#	And parameter "description" with value "This is a new MT products."
#	And parameter "model" with value "NP12244"
#	When Method "Post"
#	And Print Response

#Scenario: Dynamic headers FB (working)
#	Given Path "https://test-knowme.fbnzd.co.nz/connect/token"
#	And header "Authorization" with value "UGxhY2VNYWtlcnNUcmFkZUFwcDpGQlNlY3JldFBsYWNlTWFrZXJzVHJhZGVBcHA=" and "Content-Type" with value "application/x-www-form-urlencoded"
#	And body parameter "grant_type" with value "authorization_code"
#	And body parameter "Code" with value "914fa73f79c06bed221a72a4c9f28789"
#	And body parameter "redirect_uri" with value "fletcherbuilding://placemakers.co.nz"
#	When Method "Post"
#	And Print Response

#Scenario: Dynamic params (working)
#	Given Path "http://localhost:3030/products"
#	And parameter "name" with value "Manoj"
#	And parameter "type" with value "Hard MTT Goods"
#	And parameter "upc" with value "9898787848"
#	And parameter "price" with value 44.66
#	And parameter "description" with value "This is a new MT products."
#	And parameter "model" with value "NP12244"
#	When Method "Post"
#	And Print Response

#	| name | Manoj |
#	| type | Hard MT Goods |
#	|upc': '9898787848|
#	|price': 44.66, 'description': 'This is a new MT products.|
#	|model': 'NP12244|

#Scenario: Dynamic params (working)
#	Given Path "http://localhost:3030/products"
#	And parameter "name" with value "Jii"
#	And parameter "type" with value "Hard Jii Goods"
#	And parameter "upc" with value "9898787849"
#	And parameter "price" with value 44.66
#	And parameter "description" with value "This is a new something products."
#	And parameter "model" with value "NP1224455"
#	When Method "Post"
#	And Print Response

#Scenario: Soap Service (working)
#	Given Path "http://www.dneonline.com/calculator.asmx"
#	When service is "SOAP" with XML file path: ".\\resources\\Soap.xml"
#	Then Statuscode "200"
#	And Match header Content-type == "application/soap+xml; charset=utf-8"
#	And Match XMLPath "AddResult" contains "20"
#	And Print Response Time
#	And Print Response

#Scenario: Get all products, sort by highest price (descending) (working)
#	Given Path "http://localhost:3030/products?$sort[price]=-1" 
#	When Method "Get" 
#	Then Statuscode "200" 
#	And Match header Content-type == "application/json; charset=utf-8" 
#	And Match jsonpath "data[0].id" contains "9999848" 
#	And Print Response Time 
#	And Print Response

#@First
#Scenario: Get method (working)
#	Given Path "http://localhost:3030/products/9999922" 
#	When Method "Get"
#	Then Statuscode "200" 
#	And Match header Content-type == "application/json; charset=utf-8" 
#	And Match jsonpath "id" contains "9999922" 
#	And Print Response Time
#	And assert response time is less than 5000 milliseconds
#	And Print Response
#	And print response to file 'C:\Users\Manoj\Downloads\Output-files\reqGET.txt'

#	And File compare "C:\Users\user\Downloads\Output files\reqGET.json" with "C:\Users\user\Downloads\Output files\Sample.txt"
	
#Scenario: Post method using file (working)
#	Given Path "http://localhost:3030/products"
#	When Method "Post" and file "C:\Users\Manoj\eclipse-workspace\Gherkin_FW\resources\Post"
#	Then Statuscode "201"
#	And Match header Content-type == "application/json; charset=utf-8"
#	And Match jsonpath "upc" contains "9898787848"
#	And Print Response Time
#	And Print Response

#Scenario: Put method using file (working)
#	Given Path "http://localhost:3030/products/9999909" 
#	When Method "Put" and file "C:\Users\Manoj\eclipse-workspace\Gherkin_FW\resources\Put" 
#	Then Statuscode "200" 
#	And Match header Content-type == "application/json; charset=utf-8" 
#	And Match jsonpath "upc" contains "9898787849" 
#	And Print Response Time 
#	And Print Response
	
#Scenario: Delete method (working)
#	Given Path "http://localhost:3030/products/9999916" 
#	When Method "Delete" 
#	Then Statuscode "200" 
#	And Match header Content-type == "application/json; charset=utf-8" 
#	And Match jsonpath "id" contains "9999916" 
#	And Print Response Time 
#	And Print Response